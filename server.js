
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
require('dotenv').config({path:'./config.env'})
const { MongoClient, ObjectId } = require('mongodb');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors())

app.use(express.json());

app.use((req,res,next) => {
    console.log('Incoming Request',req.method,req.url);
    next()
})  

app.use(require('./routes/record'))
const dbo = require('./db/conn')

dbo,connectToDatabase(async function(err) {
    if (err) {
        process.exit(1)
    } else {
        const db = dbo.getDb()
        const collectionCount = await db.collection('products').countDocuments()
        if (collectionCount===0) {
            const productsData = [  {name:'Flour',price:3,description:'Everyone knows what flour is.',quantity:20,unit:"kg"},
                                    {name:'Egg',price:5,description:'Same situation here.',quantity:5,unit:"pcs"},
                                    {name:'Milk',price:12,description:'Most likely from a cow.',quantity:12,unit:"l"},
                                    {name:'Potato',price:6,description:'Absolutely irreplaceable.',quantity:10,unit:"kg"},
                                    {name:'Sugar',price:80,description:'Peak of luxury in the Eastern Bloc.',quantity:20,unit:"kg"}];
            db.collection('products').insertMany(productsData,function(err,res) {
                if (err) {
                    console.error("Error inserting ducuments:",err);
                    process.exit(1)
                } else{
                    console.log('Documents inserted',res.insertedCounts);
                }
            })
        }
    }
})


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});


process.on('SIGINT',() => {
    dbo.closeConnection();
    process.exit(0)
})